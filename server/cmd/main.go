package main

import (
	"crypto/tls"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	"tic-tac-toe.io/pkg/conf"
)

func main() {
	flag.Usage = func() {
		fmt.Fprintln(os.Stderr, "usage:", os.Args[0], `[--config <path>] Tic-Tac-Toe Server `)
		flag.PrintDefaults()
	}
	configuration := flag.String("config", "config.yml", "Configuration file in YAML format")
	flag.Parse()

	c, err := conf.ReadConfig(*configuration)
	if err != nil {
		log.Fatalf("failed to read config: %v", err)
	}

	server := &http.Server{
		Addr: ":8443",
		TLSConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
	}

	if err := server.ListenAndServeTLS(c.TLSCertPath, c.TLSKeyPath); err != nil {
		log.Fatalf("failed to initialize TLS server: %v", err)
	}
}
