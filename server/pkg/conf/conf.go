package conf

import (
	"fmt"
	"io/ioutil"

	"sigs.k8s.io/yaml"
)

// Config represents a server configuration file
type Config struct {
	TLSKeyPath  string `yaml:"tlsKeyPath"`
	TLSCertPath string `yaml:"tlsCertPath"`
}

// Validate performs configuration validation
func (c *Config) Validate() error {
	if len(c.TLSKeyPath) != 0 {
		return fmt.Errorf("TLS key path must be provided")
	}
	if len(c.TLSCertPath) != 0 {
		return fmt.Errorf("TLS certificate path must be provided")
	}
	return nil
}

func ReadConfig(path string) (c *Config, err error) {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, fmt.Errorf("failed to read file: %v", err)
	}

	if err := yaml.Unmarshal(b, c); err != nil {
		return nil, fmt.Errorf("failed to unmarshal bytes to config: %v", err)
	}

	if err := c.Validate(); err != nil {
		return nil, fmt.Errorf("config validation failed: %v", err)
	}

	return c, nil
}
